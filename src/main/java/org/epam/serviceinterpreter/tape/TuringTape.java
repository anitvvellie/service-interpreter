package org.epam.serviceinterpreter.tape;

public interface TuringTape {
    void writeRowValue(char value);

    char getRowValue();

    void gotoNextRow() throws TapeIndexOutOfBoundsException;

    void gotoPreviousRow() throws TapeIndexOutOfBoundsException;

    void increaseRowValueByOne();

    void reduceRowValueByOne();
}
