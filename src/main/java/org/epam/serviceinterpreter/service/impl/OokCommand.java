package org.epam.serviceinterpreter.service.impl;

public enum OokCommand {
    NEXT("Ook. Ook?"), PREV("Ook? Ook."), INCR("Ook. Ook."), DECR("Ook! Ook!"),
    OUT("Ook! Ook."), IN("Ook. Ook!"), START("Ook! Ook?"), END("Ook? Ook!");

    String value;

    OokCommand(String value){
        this.value = value;
    }

    public String getCommand(){
        return value;
    }
}
