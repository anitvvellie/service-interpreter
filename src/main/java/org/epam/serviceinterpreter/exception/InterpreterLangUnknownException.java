package org.epam.serviceinterpreter.exception;

public class InterpreterLangUnknownException extends RuntimeException {
    public InterpreterLangUnknownException(String msg){
        super(msg);
    }
}
